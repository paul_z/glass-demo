# README #

This repository contains a sample TwinCAT project and supporting files to implement the very new Google Glass interface demo.  Due to the nature of the demo, this repository is primarily to support Beckhoff Automation personal.  The apk file for the demo is not available on this sight, and therefore the demo will not function in it's entirety without it.  If you need the apk file, please contact myself or your local Beckhoff engineer.

All documentation is contained in the *Wiki* area.  Supporting files, other than the apk, are in the *Downloads* area.

### Who do I talk to? ###

* If you have questions, or if you'd like to contribute, please contact me via email: P.Zurlinden@beckhoff.com.