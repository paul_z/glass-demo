﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.0.25">
  <POU Name="MAIN" Id="{cebc50c2-7fb8-4deb-b609-7b95bbc03333}">
    <Declaration><![CDATA[PROGRAM MAIN
VAR
	stAxis			: AXIS_REF;
	lrStatActVel	: LREAL;
	lrStatActPos	: LREAL;
	
	iState			: INT;
	sState			: STRING;
	
	xCmdEnable		: BOOL;
	xWarnEnable		: BOOL;
	ftWarnEnable	: F_TRIG;
	
	xCmdDisable		: BOOL;
	xWarnDisable	: BOOL;
	ftWarnDisable	: F_TRIG;
	
	xCmdStartMove	: BOOL;
	xWarnStartMove	: BOOL;
	ftWarnStartMove	: F_TRIG;
	
	xWarnMoveDone	: BOOL;
	
	lrSetVel		: LREAL	:= 10.0;
	lrSetDist		: LREAL	:= 100.0;
	lrSetPos		: LREAL	:= 100.0;
	
	ifbPower		: MC_Power;
	ifbMoveAbs		: MC_MoveAbsolute;
	ifbMoveRel		: MC_MoveRelative;
	
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// Get Axis Data
stAxis.ReadStatus();
lrStatActPos	:= stAxis.NcToPlc.ActPos;
lrStatActVel	:= stAxis.NcToPlc.ActVelo;

// State Machine Controlling Axis
IF NOT ifbPower.Status THEN
	iState	:= 0;
END_IF

CASE iState OF
	0:	sState	:= 'Off';
		// Make sure no move command issued
		ifbMoveAbs.Execute	:= FALSE;
		ifbMoveRel.Execute	:= FALSE;
		
		// Cannot Disable axis that is all ready disabled
		ftWarnDisable(CLK := xWarnDisable);
		IF ftWarnDisable.Q THEN
			xCmdDisable	:= FALSE;
		END_IF

		IF xCmdDisable AND NOT xWarnDisable THEN
			xWarnDisable	:= TRUE;
		END_IF
		
		// Cannot Start Move on axis that is disabled
		ftWarnStartMove(CLK := xWarnStartMove);
		IF ftWarnStartMove.Q THEN
			xCmdStartMove	:= FALSE;
		END_IF

		IF xCmdStartMove AND NOT xWarnStartMove THEN
			xWarnStartMove	:= TRUE;
		END_IF
	
		// Power Axis
		IF xCmdEnable THEN
			ifbPower.Enable	:= TRUE;
		END_IF
		
		IF ifbPower.Status THEN
			xCmdEnable	:= FALSE;
			iState		:= 10;
		END_IF
		
	10:	sState	:= 'Ready';
	
		// Disable the axis
		IF xCmdDisable THEN
			xCmdDisable		:= FALSE;
			ifbPower.Enable	:= FALSE;
		END_IF
		
		IF NOT ifbPower.Status THEN
			iState		:= 0;
		END_IF
		
		// Cannot issue Enable Cmd to axis that is all ready enabled
		ftWarnEnable(CLK := xWarnEnable);
		IF ftWarnEnable.Q THEN
			xCmdEnable	:= FALSE;
		END_IF
		
		IF xCmdEnable AND NOT xWarnEnable THEN
			xWarnEnable	:= TRUE;
		END_IF
		
		// Move
		ifbMoveRel.Distance	:= lrSetDist;
		ifbMoveRel.Velocity	:= lrSetVel;
		lrSetPos			:= lrStatActPos + lrSetDist;
		
	(*	ifbMoveAbs.Position	:= lrSetPos;
		ifbMoveAbs.Velocity	:= lrSetVel;	*)
		
		IF xCmdStartMove THEN
			ifbMoveRel.Execute	:= TRUE;
		END_IF
		
		IF ifbMoveRel.Busy THEN
			xCmdStartMove	:= FALSE;
			iState			:= 20;
		END_IF
		
	20:	sState	:= 'Moving';
	
		// Cannot Disable axis that is moving
		ftWarnDisable(CLK := xWarnDisable);
		IF ftWarnDisable.Q THEN
			xCmdDisable	:= FALSE;
		END_IF

		IF xCmdDisable AND NOT xWarnDisable THEN
			xWarnDisable	:= TRUE;
		END_IF

		// Cannot issue Enable Cmd to axis that is all ready enabled
		ftWarnEnable(CLK := xWarnEnable);
		IF ftWarnEnable.Q THEN
			xCmdEnable	:= FALSE;
		END_IF
		
		IF xCmdEnable AND NOT xWarnEnable THEN
			xWarnEnable	:= TRUE;
		END_IF

		// Cannot Start Move on axis that is moving
		ftWarnStartMove(CLK := xWarnStartMove);
		IF ftWarnStartMove.Q THEN
			xCmdStartMove	:= FALSE;
		END_IF

		IF xCmdStartMove AND NOT xWarnStartMove THEN
			xWarnStartMove	:= TRUE;
		END_IF
		
		// Wait for finished move
		IF ifbMoveRel.Done THEN
			ifbMoveRel.Execute	:= FALSE;
			xWarnMoveDone		:= TRUE;
			iState				:= 30;
		END_IF
		
	30: sState	:= 'Done';
	
		// Cannot return to "Ready" state until xCmdStartMove is low
		ftWarnStartMove(CLK := xWarnStartMove);
		IF ftWarnStartMove.Q THEN
			xCmdStartMove	:= FALSE;
		END_IF

		IF xCmdStartMove AND NOT xWarnStartMove THEN
			xWarnStartMove	:= TRUE;
		END_IF
		
		IF (NOT xCmdStartMove) AND (NOT xWarnMoveDone) THEN
			iState	:= 10;
		END_IF
	
END_CASE

// Motion FBs
ifbPower(
	Axis:= stAxis, 
	Enable_Positive:= TRUE, 
	Enable_Negative:= TRUE);
	
	
ifbMoveAbs(
	Axis:= stAxis, 
	Position:= lrSetPos, 
	Velocity:= lrSetVel);
	
ifbMoveRel(
	Axis:= stAxis);]]></ST>
    </Implementation>
    <ObjectProperties />
  </POU>
</TcPlcObject>